/*********
  Lael Ramos - Sizenando Braga
  Estufa Eletrônica
*********/

// Livrarias
#include "WiFi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"
#include <Adafruit_Sensor.h>
#include <DHT.h>

// Credencias de conexão
char* ssid     = "CleanNet_9651";
char* password = "95473725";

//  GPIO
const int ledPin  = 2;
const int ledPin2 = 4;
const int ledPin3 = 33;
#define   DHTPIN    23     // Digital pin connected to the DHT sensor

// Stores LED state
String ledState;

// Escolher o sensor em uso:
#define DHTTYPE    DHT11     // DHT 11
//#define DHTTYPE    DHT22     // DHT 22 (AM2302)

// Objeto dht
DHT dht(DHTPIN, DHTTYPE);

// Objeto AsyncWebServer no port 80
AsyncWebServer server(80);


String readDHTTemperature() {
  //As leituras dos sensores também podem ser de até 2 segundos

  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  //float t = dht.readTemperature(true);
  // Check if any reads failed and exit early (to try again).
  if (isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return "--";
  }
  else {
    Serial.println(t);
    return String(t);
  }
}

String readDHTHumidity() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  if (isnan(h)) {
    Serial.println("Falha na leitura do sensor!");
    return "--";
  }
  else {
    Serial.println(h);
    return String(h);
  }
}



// Replaces placeholder with LED state value
String processor(const String& var) {
  //Serial.println(var);
  if (var == "TP") {
    return readDHTTemperature();
  }
  else if (var == "HAR") {
    return readDHTHumidity();
  }
  return String();
}

void setup() {
  // Inicia a o Serial port para debug
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  dht.begin();

  // Inicia o protocolo SPIFFS
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Connecta ao Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Printa o endereço IP Local do ESP32
  Serial.println(WiFi.localIP());

  // Rota para raiz / página web
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  // Rota para carregar o arquivo style.css
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/style.css", "text/css");
  });
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Rota para definir GPIO em HIGH
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest * request) {
    digitalWrite(ledPin, HIGH);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  server.on("/on2", HTTP_GET, [](AsyncWebServerRequest * request) {
    digitalWrite(ledPin2, HIGH);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  server.on("/on3", HTTP_GET, [](AsyncWebServerRequest * request) {
    digitalWrite(ledPin3, HIGH);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });


  // Rota para definir GPIO em LOW
  server.on("/off", HTTP_GET, [](AsyncWebServerRequest * request) {
    digitalWrite(ledPin, LOW);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  server.on("/off2", HTTP_GET, [](AsyncWebServerRequest * request) {
    digitalWrite(ledPin2, LOW);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  server.on("/off3", HTTP_GET, [](AsyncWebServerRequest * request) {
    digitalWrite(ledPin3, LOW);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Start server
  server.begin();
}

void loop() {

}
